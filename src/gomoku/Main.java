package gomoku;

import java.util.Scanner;

/**
 *
 * @author Long Ta
 */
public class Main {

	public enum PLAYER {
		HUMAN, AI
	}

	private static void printOptions() {
		System.out.println("1 -> Human");
		System.out.println("2 -> AI");
	}

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String[] args) {

		Scanner myScan = new Scanner(System.in);
		PLAYER p1 = PLAYER.HUMAN;
		PLAYER p2 = PLAYER.AI;
		int option;

		System.out.printf("Welcome to Gomoku\n\n\n");
		System.out.printf("Choose first player:\n\n");
		printOptions();
		option = myScan.nextInt();
		if (option == 2){
		    p1 = PLAYER.AI;
		    p2 = PLAYER.HUMAN;
		}

		playGame(p1, p2);
		System.out.printf("\n\nGoodbye!\n\n");
	}

	private static void playGame(PLAYER p1, PLAYER p2) {
		int width = 20;
		int height = 20;
		Table gameTable = new Table(width, height);
		int win = 0;
		boolean validMove = false;
		int[] move = new int[2];

		Player player1 = new Player(1);
		Player player2 = new Player(2);

		// playerOne
		if (p1 == PLAYER.HUMAN) {
			player1.setID(PLAYER.HUMAN);
		} else if (p1 == PLAYER.AI) {
			player1.setID(PLAYER.AI);
		}

		// playerTwo
		if (p2 == PLAYER.HUMAN) {
			player2.setID(PLAYER.HUMAN);
		} else if (p2 == PLAYER.AI) {
			player2.setID(PLAYER.AI);
		}

		boolean firstMove = true;
		
		if (p1.equals(PLAYER.HUMAN)){
		    firstMove = false;
		}
		while (true) {
		    
			// p1 move
			gameTable.printTable();
			while (!validMove) {
				move = player1.getMove(gameTable.getStore(),firstMove);
				validMove = gameTable.makeMove(1, move[0], move[1]);
			}
			validMove = false;

			// check win
			win = gameTable.checkWin();
			if (win == 1) {
				gameTable.printTable();
				System.out.println("Player one Wins");
				break;
			}

			// p2 move
			gameTable.printTable();
			while (!validMove) {
				move = player2.getMove(gameTable.getStore(),firstMove);
				validMove = gameTable.makeMove(2, move[0], move[1]);
			}
			firstMove = false;
			validMove = false;

			// check win
			win = gameTable.checkWin();
			if (win == 2) {
				gameTable.printTable();
				System.out.println("Player two Wins");
				break;
			}
			
		}

	}
}